# Eier
# Copyright (c) 2005, Patrick Gerdsmeier <patrick@gerdsmeier.net>

CACHE	 = # ccache # use http://ccache.samba.org to speedup compiling
CXX      = $(CACHE) g++
CXXFLAGS = -g -O3 -Wall `sdl-config --cflags`
LDFLAGS  =
LIBS     = -L. `sdl-config --libs` # -lSDL_image
SRCDIR   = src
BIN      = eier

OBJS = 	$(SRCDIR)/main.o $(SRCDIR)/graphics.o

all:	$(BIN)

$(BIN):	$(OBJS)
	$(CXX) $(CXXFLAGS) $(LDFLAGS) -o $(BIN) $(OBJS) $(LIBS)

clean:        
	rm -f $(SRCDIR)/*.o
	rm -f $(SRCDIR)/*~
	rm -f *~

new: 	clean all
